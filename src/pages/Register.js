import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {  Navigate, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";

export default function Register(){

	const {user} = useContext(UserContext);

    const navigate = useNavigate();

    //State hooks to store the values of input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    function registerUser(event) {
        event.preventDefault();
        
            
            // if email doesn't exist, register the user
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
              method: "POST",
              headers: { "Content-type": "application/json" },
              body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNumber: mobileNumber,
                password: password1
              })
            })
              .then((res) => res.json())
              .then((data) => {
                console.log(data)

                // show a success message if registration is successful
                if (data.email) {
                    Swal.fire({
                        title: "Duplicate Email Found",
                        icon: "error",
                        text: "Please provide a different email",
                    })
                }
                else{
                  Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!",
                  });
                  navigate("/login")
                }
              })
              // clear input fields
              setFirstName("");
              setLastName("");
              setEmail("");
              setMobileNumber("");
              setPassword1("");
              setPassword2("");
      }        


    useEffect(() => {

        if((firstName !== '' && lastName !== '' && email !== '' && mobileNumber !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [firstName, lastName, email, mobileNumber, password1, password2])

    // const { user, setUser } = useContext(UserContext);

    return(
        (user.id !== null)?
        <Navigate to = "/login"/>
        :
        <Form onSubmit={(event) => registerUser(event)}>
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="firstName" 
                    placeholder="First Name" 
                    value = {firstName}
                    onChange = {event => setFirstName(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="lastName" 
                    placeholder="Last Name" 
                    value = {lastName}
                    onChange = {event => setLastName(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value = {email}
                    onChange = {event => setEmail(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="mobileNumber" 
                    placeholder="Mobile Number" 
                    value = {mobileNumber}
                    onChange = {event => setMobileNumber(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value = {password1}
                    onChange = {event => setPassword1(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value = {password2}
                    onChange = {event => setPassword2(event.target.value)}
                    required
                />
            </Form.Group>

            <div className='pt-3'>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </div>
        </Form>
        )
    }
