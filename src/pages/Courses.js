import coursesData from "../data/coursesData"
import CourseCard from "../components/CourseCard";
import React, { Fragment, useContext, useState, useEffect } from "react";
import UserContext from "../UserContext";
import {Navigate} from 'react-router-dom'

export default function Courses(){
    console.log("Contents of coursesData: ");
    console.log(coursesData);
    console.log(coursesData[0]);


    const {user} = useContext(UserContext)

    const [courses, setCourses] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/`)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setCourses(data.map(course => {
                return(
                    <CourseCard key={course._id} courseProp = {course}/>
                )
            }));
        })
    }, []);

    // array method to display all courses
    // const courses = coursesData.map(course => {
    //     return (
    //         <CourseCard key={course.id} courseProp = {course}/>
    //     )
    // })

    // return(
    //     <React.Fragment>
    //         <CourseCard courseProp={coursesData[0]}/>
            
    //     </React.Fragment>
    // )

    return (
        (user.isAdmin)?
            <Navigate to = "/admin" />
        :
        <>
            <h1>Courses</h1>
            {courses}
        </>
    )
}