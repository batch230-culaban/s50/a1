import { Fragment } from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
// import CourseCard from "../components/CourseCard";

const data = {
    title: "Zuitt Coding Bootcamp",
    conent: "Opportunities for everyone, everywhere.",
    destination: "/",
    label: "Enroll Now!"
}

export default function Home() {
    return (
        <Fragment>
            <Banner data={data}/>
            <Highlights/>
            {/* <CourseCard/> */}
        </Fragment>
    )
}