import Banner from '../components/Banner';


export default function Error(){

    const data = {
        title: "404 - Not Found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back Home"
    }

    return(
        <Banner data={data}/>
    )
    // My solution
    // return(
    //     <Fragment>
    //     <h1>Page Not Found</h1>
    //     <h4>Go back to the <Link to="/">homepage</Link></h4>
    //     </Fragment>
    // )
}

