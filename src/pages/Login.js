import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login() {
    // State hooks
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    // Variable, setter function
    // 3 - state
    const { user, setUser } = useContext(UserContext);
    
    /*
      function authentication(event){

            // event.preventDefault();
            localStorage.setItem('email', email);

            setUser({
              email: localStorage.setItem('email')
            })

            setEmail('');
            setPassword('');
            
            console.log(`${email} has been verified! Welcome Back!`)
            alert('Login Successfully!');
      }*/


 function authenticate(e){
  e.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    method: 'POST',
    headers: {"Content-type" : 'application/json'},
    body: JSON.stringify({
      email: email,
      password: password
    })
  })
  .then(rest => rest.json())
  .then(data => {
    console.log(data);
    console.log("Check accessToken");
    console.log(data.accessToken);
    

    if(typeof data.accessToken !== "undefined"){
      localStorage.setItem('token', data.accessToken);
      retrieveUserDetails(data.accessToken);

      Swal.fire({
        title: "Login Successful",
        icon: "success",
        text: "Welcome to Zuitt!"
      })
    }
      else{
        Swal.fire({
          title: "Authentication failed",
          icon: "error",
          text: "Check your login details and try again"
        })
      }
    })
  setEmail('');
 }
 
 const retrieveUserDetails = (token) => {
  fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
          Authorization: `Bearer ${token}`
      }
  })
  .then(res => res.json())
  .then(data => {
      console.log(data);

      setUser({
          email: data.email,
          id: data._id,
          isAdmin: data.isAdmin
      })
  })
}


 useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true)
        }
        else{
            setIsActive(false)
        }
    }, [email, password])





  return (
    (user.id !== null)?
    // true - means email field is successfully set
    <Navigate to ="/courses"/>
    : // false - means email field is not successfully set
    <Form onSubmit={(event) => authenticate(event)}>
        <h1>Login</h1>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" 
        value = {email} 
        onChange = {e => setEmail(e.target.value)}/>
        {/* <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text> */}
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" 
        value = {password} 
        onChange = {e => setPassword(e.target.value)}/>
      </Form.Group>

      {isActive ? 
      <Button variant="success" type="submit">
        Login
      </Button>
      :
      <Button variant="success" type="submit" disabled>
        Login
      </Button>
        }
    </Form>
  );
}