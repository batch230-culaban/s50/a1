import { Card, Button } from 'react-bootstrap';
import { useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

// export default function CourseCard({courseProp}) {

// 	// console.log("Contents of props: ");
// 	// console.log(props);
// 	// console.log(typeof props);
    
// 	return (
// 	    <Card>
// 	        <Card.Body>
// 	            <Card.Title>{courseProp.name}</Card.Title>
// 	            <Card.Subtitle>Description:</Card.Subtitle>
// 	            <Card.Text>{courseProp.description}</Card.Text>
// 	            <Card.Subtitle>Price:</Card.Subtitle>
// 	            <Card.Text>{courseProp.price}</Card.Text>
// 	            <Button variant="primary">Enroll</Button>
// 	        </Card.Body>
// 	    </Card>
// 	)
// }


// SHORTCUT to make it Simplier

export default function CourseCard({courseProp}) {

    const {_id, name, description, price} = courseProp;

// State Hooks (useState) - a way to store information within a component and track this information

    // const [count, setCount] = useState(0); // count = 0;
    // const [slots, setSlots] = useState(30);

    // // function enroll() {
    // //     // if (slots <=0){
    // //     //     alert('No More Seats Available')
    // //     // }
    // //     // else{
    // //     //     setSlots(slots - 1);
    // //     //     setCount(count + 1);
    // //     //     console.log('Enrollees: ' + count);
    // //     // }

    // //         setSlots(slots - 1);
    // //         setCount(count + 1);
    // //         console.log('Enrollees: ' + count);
    // // }

    // // UseEffect() always runs the task on the initial render and/or every render (when the state changes in a component)
    // // Initial render is when the component is run or displayed for the first time
    // useEffect(() => {
    //     if(slots < 0){
    //         alert('No More Seats Available')
    //     }
    // }, [slots])
        

	return (
	    <Card>
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Button as={Link} to={`/courses/${_id}`}>Enroll</Button>
                {/* <Card.Text>Total Enrolled: {count} <br/> Seats Available: {slots}</Card.Text> */}
	        </Card.Body>
	    </Card>
	)
}

// Check if the coursecard component is getting the correct property types
// CourseCard.propTypes = {
//     courseProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired,
//     })
// }