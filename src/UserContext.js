import React from "react";


// A context object with object data type taht can be used to store information that can be share to other components within the application

const UserContext = React.createContext();
// console.log("Contents of UserContext")
// console.log(UserContext)

// The "Provider" component allows other components to consume/use the context object and supply the necessary information needed in the context object

export const UserProvider = UserContext.Provider;

export default UserContext;